﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NataliaTemplateProject.Models;

namespace NataliaTemplateProject.Controllers
{
    public class AddressController : Controller
    {
        static readonly IAddress repository = new AddressRepository();
        static readonly IEmployee emprepository = new EmployeeRepository();
        // GET: Address
        public ActionResult Index()
        {
            var address = repository.GetAll();
            return View(address);
        }

        // GET: Address/Details/5
        public ActionResult Details(int id)
        {
            Address address = repository.Get(id);
            if (address == null)
            {
                Response.StatusCode = 404;
            }
            return View(address);
        }

        // GET: Address/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Address/Create
        [HttpPost]
        public ActionResult Create(Address address)
        {
            if (ModelState.IsValid)
            {
                int addressid = repository.Add(address);
                if (addressid > 0)
                {
                    return RedirectToAction("Index", "Address");
                }
                else
                {
                    ModelState.AddModelError("", "Can Not Insert");
                }
            }
            return View(address);
        }

        // GET: Address/Edit/5
        public ActionResult Edit(int id)
        {
            Address address = repository.Get(id);
            return View(address);
        }

        // POST: Address/Edit/5
        [HttpPost]
        public ActionResult Edit(Address address)
        {
            int records = repository.Update(address);
            if (records > 0)
            {
                return RedirectToAction("Index", "Address");
            }
            else
            {
                ModelState.AddModelError("", "Can Not Delete");
                return View("Index");
            }
        }

        // GET: Address/Delete/5
        public ActionResult Delete(int id)
        {
            Address address = repository.Get(id);
           
            return View(address);
        }

        // POST: Address/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            bool empany = emprepository.GetAll().Where(s => s.AddressId == id).Any();
            if (empany)
            {
                ViewBag.ErrorMessage = "Cannot delete. Employee is assigned.";
                Address address = repository.Get(id);
                return View(address);
            }
            else
            {
                int records = repository.Remove(id);
                if (records > 0)
                {
                    return RedirectToAction("Index", "Address");
                }
                else
                {
                    ModelState.AddModelError("", "Can Not Delete");
                    return View("Index");
                }
            }
        }
    }
}

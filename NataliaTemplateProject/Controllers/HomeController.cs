﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NataliaTemplateProject.Models;

namespace NataliaTemplateProject.Controllers
{
    public class HomeController : Controller
    {
        static readonly IEmployee emprepository = new EmployeeRepository();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Templates()
        {
            ViewBag.Message = "Templates to add information.";
            Employee emp = new Employee();
            ViewBag.Employee = emp;
            Address address = new Address();
            ViewBag.Address = address;
            Family family = new Family();
            ViewBag.Family = family;
            var employees = emprepository.GetAll();
            ViewBag.Employees = employees;
            return View();
        }

        public ActionResult Information()
        {
            ViewBag.Message = "View saved information.";

            return View();
        }
    }
}
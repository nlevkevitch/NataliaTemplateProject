﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NataliaTemplateProject.Models;

namespace NataliaTemplateProject.Controllers
{
    public class FamilyController : Controller
    {
        static readonly IFamily repository = new FamilyRepository();
        static readonly IEmployee emprepository = new EmployeeRepository();

        // GET: Address
        public ActionResult Index()
        {
            var family = repository.GetAll();
            return View(family);
        }
        // GET: Address/Details/5
        public ActionResult Details(int id)
        {
            Family family = repository.Get(id);
            if (family == null)
            {
                Response.StatusCode = 404;
            }
            return View(family);
        }

        // GET: Address/Create
        public ActionResult Create()
        {
            var employees = emprepository.GetAll();
            ViewBag.Employees = employees;
            return View();
        }

        // POST: Address/Create
        [HttpPost]
        public ActionResult Create(Family family)
        {
            if (ModelState.IsValid)
            {
                int rows = repository.Add(family);
                if (rows > 0)
                {
                    return RedirectToAction("Index", "Family");
                }
                else
                {
                    ModelState.AddModelError("", "Can Not Insert");
                }
            }
            return View(family);
        }

        // GET: Address/Edit/5
        public ActionResult Edit(int id)
        {
            Family family = repository.Get(id);
            var employees = emprepository.GetAll();
            ViewBag.Employees = employees;
            return View(family);
        }

        // POST: Address/Edit/5
        [HttpPost]
        public ActionResult Edit(Family family)
        {
            int records = repository.Update(family);
            if (records > 0)
            {
                return RedirectToAction("Index", "Family");
            }
            else
            {
                ModelState.AddModelError("", "Can Not Delete");
                return View("Index");
            }
        }

        // GET: Address/Delete/5
        public ActionResult Delete(int id)
        {
            Family family = repository.Get(id);
            return View(family);
        }

        // POST: Address/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            int records = repository.Remove(id);
            if (records > 0)
            {
                return RedirectToAction("Index", "Family");
            }
            else
            {
                ModelState.AddModelError("", "Can Not Delete");
                return View("Index");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NataliaTemplateProject.Models;


namespace NataliaTemplateProject.Controllers
{
    public class EmployeeController : Controller
    {
        static readonly IEmployee repository = new EmployeeRepository();
        static readonly IAddress addressrep = new AddressRepository();

        // GET: Employee
        public ActionResult Index()
        {
            var employee = repository.GetAll();
            return View(employee);
        }

        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            Employee emp = repository.Get(id);
            if (emp == null)
            {
                Response.StatusCode = 404;
            }
            return View(emp);
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(Employee emp)
        {
            if (ModelState.IsValid)
            {
                int rows = repository.Add(emp);
                if (rows > 0)
                {
                    return RedirectToAction("Index", "Employee");
                }
                else
                {
                    ModelState.AddModelError("", "Can Not Insert");
                }
            }
            return View(emp);
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            Employee emp = repository.Get(id);
            
            return View(emp);
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(Employee emp)
        {
            int records = repository.Update(emp);
            if (records > 0)
            {
                return RedirectToAction("Index", "Employee");
            }
            else
            {
                ModelState.AddModelError("", "Can Not Delete");
                return View("Index");
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            Employee emp = repository.Get(id);
            return View(emp);
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            int records = repository.Remove(id);
            if (records > 0)
            {
                return RedirectToAction("Index", "Employee");
            }
            else
            {
                ModelState.AddModelError("", "Can Not Delete");
                return View("Index");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NataliaTemplateProject.Models
{
    public interface IAddress
    {
        IEnumerable<Address> GetAll();
        Address Get(int Id);
        int Add(Address item);
        int Remove(int Id);
        int Update(Address item);
    }
}

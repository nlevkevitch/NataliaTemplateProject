﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NataliaTemplateProject.Models
{
    public interface IFamily
    {
        IEnumerable<Family> GetAll();
        Family Get(int Id);
        int Add(Family item);
        int Remove(int Id);
        int Update(Family item);
    }
}

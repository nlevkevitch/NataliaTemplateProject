﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NataliaTemplateProject.Models
{
    public interface IEmployee
    {
        IEnumerable<Employee> GetAll();
        Employee Get(int Id);
        int Add(Employee item);
        int Remove(int Id);
        int Update(Employee item);
    }
}

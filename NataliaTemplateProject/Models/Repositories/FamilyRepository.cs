﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace NataliaTemplateProject.Models
{
    public class FamilyRepository : IFamily
    {
        public string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public Family Get(int id)
        {
            var model = new Family();
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "Select * from Family JOIN Employee ON Family.EmployeeId = Employee.Id where Family.Id=@id";
                    cmd.Parameters.AddWithValue("@Id", id);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        model.Id = int.Parse(reader["Id"].ToString());
                        model.FirstName = reader["FFirstName"].ToString();
                        model.MiddleName = reader["FMiddleName"].ToString();
                        model.LastName = reader["FLastName"].ToString();
                        model.Relashionship = reader["Relashionship"].ToString();
                        model.EmployeeId = int.Parse(reader["EmployeeId"].ToString());
                        model.Employee = new Employee();                 
                        model.Employee.FirstName = reader["FirstName"].ToString();
                        model.Employee.MiddleName = reader["MiddleName"].ToString();
                        model.Employee.LastName = reader["LastName"].ToString();
                        model.Employee.Position = reader["Position"].ToString();
                        model.Employee.Age = int.Parse(reader["Age"].ToString());
                    }
                }
            }
            return model;
        }
        public IEnumerable<Family> GetAll()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "Select * from Family JOIN Employee ON Family.EmployeeId = Employee.Id";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            // Create a Favorites instance
                            var model = new Family();
                            model.Id = int.Parse(reader["Id"].ToString());
                            model.FirstName = reader["FFirstName"].ToString();
                            model.MiddleName = reader["FMiddleName"].ToString();
                            model.LastName = reader["FLastName"].ToString();
                            model.Relashionship = reader["Relashionship"].ToString();
                            model.EmployeeId = int.Parse(reader["EmployeeId"].ToString());
                            model.Employee = new Employee();
                            model.Employee.FirstName = reader["FirstName"].ToString();
                            model.Employee.MiddleName = reader["MiddleName"].ToString();
                            model.Employee.LastName = reader["LastName"].ToString();
                            model.Employee.Position = reader["Position"].ToString();
                            model.Employee.Age = int.Parse(reader["Age"].ToString());
                            yield return model;
                        }
                    }
                }
            }
        }
        public int Add(Family family)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "insert into Family(FFirstName, FMiddleName, FLastName, Relashionship, EmployeeId)values(@FirstName, @MiddleName, @LastName, @Relashionship, @EmployeeId)";
                    command.Parameters.AddWithValue("@FirstName", family.FirstName);
                    command.Parameters.AddWithValue("@MiddleName", family.MiddleName??string.Empty);
                    command.Parameters.AddWithValue("@LastName", family.LastName);
                    command.Parameters.AddWithValue("@Relashionship", family.Relashionship);
                    command.Parameters.AddWithValue("@EmployeeId", family.EmployeeId);
                    return command.ExecuteNonQuery();
                }
            }
        }
        public int Update(Family family)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "update Family set FFirstName=@FirstName,FMiddleName=@MiddleName,FLastName=@LastName,Relashionship=@Relashionship,EmployeeId=@EmployeeId where Id=@Id";
                    command.Parameters.AddWithValue("@Id", family.Id);
                    command.Parameters.AddWithValue("@FirstName", family.FirstName);
                    command.Parameters.AddWithValue("@MiddleName", family.MiddleName ?? string.Empty);
                    command.Parameters.AddWithValue("@LastName", family.LastName);
                    command.Parameters.AddWithValue("@Relashionship", family.Relashionship);
                    command.Parameters.AddWithValue("@EmployeeId", family.EmployeeId);
                    return command.ExecuteNonQuery();
                }
            }
        }
        public int Remove(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "delete from Family where Id=@Id";
                    command.Parameters.AddWithValue("@Id", id);
                    return command.ExecuteNonQuery();
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace NataliaTemplateProject.Models
{
    public class EmployeeRepository : IEmployee
    {
        public string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public Employee Get(int id)
        {
            var model = new Employee();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select * from Employee LEFT JOIN Address on Employee.AddressId = Address.Id where Employee.Id=@Id";
                    command.Parameters.AddWithValue("@Id", id);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        model.Id = int.Parse(reader["Id"].ToString());
                        model.FirstName = reader["FirstName"].ToString();
                        model.MiddleName = reader["MiddleName"].ToString();
                        model.LastName = reader["LastName"].ToString();
                        model.Position = reader["Position"].ToString();
                        model.Age = int.Parse(reader["Age"].ToString());
                        int tempVal;
                        model.AddressId = Int32.TryParse(reader["AddressId"].ToString(), out tempVal) ? tempVal : (int?)null;
                        if (model.AddressId != null)
                        {
                            model.Address = new Address();
                            model.Address.City = reader["City"].ToString();
                            model.Address.Street = reader["Street"].ToString();
                            model.Address.State = reader["State"].ToString();
                            model.Address.ZipCode = reader["ZipCode"].ToString();
                        }
                    }
                }
            }
            return model;
        }
        public IEnumerable<Employee> GetAll()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "Select * from Employee LEFT JOIN Address on Employee.AddressId = Address.Id ";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            // Create a Favorites instance
                            var model = new Employee();
                            model.Id = int.Parse(reader["Id"].ToString());
                            model.FirstName = reader["FirstName"].ToString();
                            model.MiddleName = reader["MiddleName"].ToString();
                            model.LastName = reader["LastName"].ToString();
                            model.Position = reader["Position"].ToString();
                            model.Age = int.Parse(reader["Age"].ToString());
                            int tempVal;
                            model.AddressId = Int32.TryParse(reader["AddressId"].ToString(), out tempVal) ? tempVal : (int?)null;
                            if (model.AddressId != null)
                            {
                                model.Address = new Address();
                                model.Address.City = reader["City"].ToString();
                                model.Address.Street = reader["Street"].ToString();
                                model.Address.State = reader["State"].ToString();
                                model.Address.ZipCode = reader["ZipCode"].ToString();
                            }
                            yield return model;
                        }
                    }
                }
            }
        }
        public int Add(Employee emp)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "insert into Employee(FirstName, MiddleName, LastName, Position, Age)values(@FirstName, @MiddleName, @LastName, @Position, @Age)";
                    command.Parameters.AddWithValue("@FirstName", emp.FirstName);
                    command.Parameters.AddWithValue("@MiddleName", emp.FirstName);
                    command.Parameters.AddWithValue("@LastName", emp.LastName);
                    command.Parameters.AddWithValue("@Position", emp.Position);
                    command.Parameters.AddWithValue("@Age", emp.Age);
                    return command.ExecuteNonQuery();
                }
            }
        }
        public int Update(Employee emp)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "update Employee set FirstName=@FirstName,MiddleName=@MiddleName,LastName=@LastName,Position=@Position,Age=@Age,AddressId=@AddressId where Id=@Id";
                    command.Parameters.AddWithValue("@Id", emp.Id);
                    command.Parameters.AddWithValue("@FirstName", emp.FirstName);
                    command.Parameters.AddWithValue("@MiddleName", emp.FirstName);
                    command.Parameters.AddWithValue("@LastName", emp.LastName);
                    command.Parameters.AddWithValue("@Position", emp.Position);
                    command.Parameters.AddWithValue("@Age", emp.Age);
                    command.Parameters.AddWithValue("@AddressId", emp.AddressId?? Convert.DBNull);
                    return command.ExecuteNonQuery();
                }
            }
        }
        public int Remove(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "delete from Employee where Id=@Id";
                    command.Parameters.AddWithValue("@Id", id);
                    return command.ExecuteNonQuery();
                }
            }
        }
    }
}
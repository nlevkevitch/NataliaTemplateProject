﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace NataliaTemplateProject.Models
{
    public class AddressRepository: IAddress
    {
        public string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public Address Get(int id)
        {
            var model = new Address();
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "Select * from Address where Id=@id";
                    cmd.Parameters.AddWithValue("@Id", id);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        model.Id = int.Parse(reader["Id"].ToString());
                        model.Street = reader["Street"].ToString();
                        model.City = reader["City"].ToString();
                        model.State = reader["State"].ToString();
                        model.ZipCode = reader["ZipCode"].ToString();
                    }
                }
            }
            return model;
        }
        public IEnumerable<Address> GetAll()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "Select * from Address";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            // Create a Favorites instance
                            var model = new Address();
                            model.Id = int.Parse(reader["Id"].ToString());
                            model.Street = reader["Street"].ToString();
                            model.City = reader["City"].ToString();
                            model.State = reader["State"].ToString();
                            model.ZipCode = reader["ZipCode"].ToString();
                            yield return model;
                        }
                    }
                }
            }
        }
        public int Add(Address address)
        {
            int id = -1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "insert into Address(Street, City, State, ZipCode)values(@Street, @City, @State, @ZipCode) set @AddressId = SCOPE_IDENTITY()";
                    command.Parameters.AddWithValue("@Street", address.Street);
                    command.Parameters.AddWithValue("@City", address.City);
                    command.Parameters.AddWithValue("@State", address.State);
                    command.Parameters.AddWithValue("@ZipCode", address.ZipCode);
                    command.Parameters.Add("@AddressId", SqlDbType.Int, 0, "Id");
                    command.Parameters["@AddressId"].Direction = ParameterDirection.Output;
                    command.ExecuteNonQuery();
                    id = (int)command.Parameters["@AddressId"].Value;
                    return id;
                }
            
            }
        }
        public int Update(Address address)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "update Address set Street=@Street,City=@City,State=@State,ZipCode=@ZipCode where Id=@Id";
                    command.Parameters.AddWithValue("@Id", address.Id);
                    command.Parameters.AddWithValue("@Street", address.Street);
                    command.Parameters.AddWithValue("@City", address.City);
                    command.Parameters.AddWithValue("@State", address.State);
                    command.Parameters.AddWithValue("@ZipCode", address.ZipCode);
                    return command.ExecuteNonQuery();
                }
            }
        }
        public int Remove(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "delete from Address where Id=@Id";
                    command.Parameters.AddWithValue("@Id", id);
                    return command.ExecuteNonQuery();
                }
            }
        }
    }
}